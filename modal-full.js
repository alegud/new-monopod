$(document).ready(function() {
	$('a[name=modal-full]').click(function(e) {
		e.preventDefault();
		var id = $(this).attr('href');
		var maskHeight = $(document).height();
		var maskWidth = $(window).width();
		$('#mask-full').css({'width':maskWidth,'height':maskHeight});
		$('#mask-full').fadeIn(1000);
		$('#mask-full').fadeTo("slow",0.8);
		var winH = $(window).height();
		var winW = $(window).width();
		$(id).css('top',  winH/2-$(id).height()/2);
		$(id).css('left', winW/2-$(id).width()/2);
		$(id).fadeIn(2000);
	});
	$('.window-full .close-full').click(function (e) {
		e.preventDefault();
		$('#mask-full, .window-full').hide();
	});
	$('#mask-full').click(function () {
		$(this).hide();
		$('.window-full').hide();
	});
});