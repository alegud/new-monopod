<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="description" content="Монопод с кнопкой Bluetooth z07-5 для идеальных селфи. Монопод с кнопкой на ручке можно купить в нашем интернет магазине по специальной цене с доставкой по Москве и другим регионам России.">
<meta name="keywords" content="монопод с Bluetooth, монопод с блютус, монопод с кнопкой, MONOPOD, z07-5, монопод для селфи, монопод для iPhone, монопод для Apple, монопод для Android, купить монопод в Москве, монопод для телефона, ЛюблюСелфи, сделать селфи, монопод с кнопкой на ручке">
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="shortcut icon" href="images/favicon.ico">
<link rel="stylesheet" href="css/style4.css">
<!--[if lte IE 8]>
<link rel="stylesheet" href="css/style4ie.css">
<![endif]-->
<!--[if gte IE 9]>
<link rel="stylesheet" href="css/style4ieM.css">
<![endif]-->
<title>Монопод z07-5 c  Bluetooth - универсальный штатив для селфи</title>

<script type="text/javascript" src="jquery-1.7.2.min.js"></script>
<script src="jquery.validate.js"></script>

<script type="text/javascript" src="slider.js"></script>
<script type="text/javascript" src="modal.js"></script>
<script type="text/javascript" src="modal-full.js"></script>
<!--<script type="text/javascript" src="down.js"></script>-->
<script type="text/javascript" src="ScrollTo.js"></script>
<script>
	$(document).ready(function() {
		$("#commentForm").validate();
		$("#commentForm2").validate();
	});
</script>
<script type="text/javascript" src="countdown.js"></script>
</head>


<body>


<ul id="scrollPanel" class="raised">
	<li><a href="#opisinie">Описание</a></li>
	<li><a href="#ucl">Бонусы</a></li>
	<li><a href="#video">Видео</a></li>
	<li><a href="#otzivi">Отзывы</a></li>
	<li><a href="#charakteristiki">Характеристики</a></li>
	<li><a href="#work">Как пользоваться?</a></li>
	<li><a href="#buy">Покупка и доставка</a></li>
	<li><a href="#guarantee">Гарантия</a></li>

</ul>


<div class="content">

	<div class="header">
	
	<div style="position: absolute; margin-left: 25px; margin-top: -5px;"><img src="images/phone_logo.png" width="250px"></div>
	
		<a href="/" class="site-name">Монопод c Bluetooth</a><br />
		<div class="contacts-header-work" style="color: #cc6600">Работаем без выходных с 8:00 до 23:00<br />Тел. 8 (499) 346-69-35</div>
		
	</div>
	
	<div style="position: absolute; margin-left: 840px; margin-top: -140px;"><img src="images/logo_head.png" width="250px"></div>

<a href="#dialog" name="modal"><div class="button-zakaz-right"></div></a>
<div id="boxes">
	<div id="dialog" class="window">
	<div class="call-form-fon1">
<div align="center" style="font-size: 27px; text-shadow: 0 1px #DDD;"><b>Заказать звонок сейчас</b></div><br />

<form class="cmxform" id="commentForm" method="post" action="call.php" onsubmit="yaCounter28059519.reachGoal('Order'); return true;">
				<label>Ваше имя: </label>
				<input id="cename" name="name" class="winput" data-rule-required="true" data-msg-required="Пожалуйса введите Ваше имя" data-msg-name="Пожалуйса введите Ваше имя">
                <div class="call-name">Пример: Николай</div>
				
				<label>Ваш номер телефона: </label>
				<input id="cephone" name="phone" class="winput" data-rule-required="true" data-msg-required="Пожалуйса введите Ваш номер телефона" data-msg-phone="Пожалуйса введите Ваш номер телефона">
                <div class="call-phone">Пример: +7985671526</div>

				<input type="submit" class="send-zakaz" value="Заказать!" />

	</form></div>	<a href="#" class="close">Закрыть</a>
	</div>
	<div id="mask"></div>
</div>
<!-- Конец кода - Модальное окно с формой заказа обратного звонка -->



<div class="header-info">
	<div class="podarki">
		<div class="podarki-title">Покупая Монопод z07-5,<br /> получите в подарок:</div>
		<div class="podarki-text">
			1.	Программируемую <br />&nbsp; &nbsp; &nbsp;чудо-кнопку IKey
		</div>
		<div class="podarki-text">
			2.	Экономию в 300 руб.
		</div>
	</div>
	<div class="header-info-left">
		<div class="src-header">
			<img src="/screen/src_header.png" />
					</div>
	</div>
	<div class="header-info-right">
				
		<div class="slogan-right">
			<b>Запечатлите все<br />лучшие моменты,<br />
			благодаря Моноподу z07-5</b>
		</div>
		

	
<!-- Модальное окно с формой заказа товара -->

<div class="header-call-form">
<div class="call-form-fon">
<div align="center" style="font-size: 33px; text-shadow: 0 1px #DDD;"><b>Заказать товар сейчас</b></div>
<!--<div>Для заказа товара нажмите кнопку "Заказать!", укажите свои данные, выберите удобный для Вас вариант получения товара, ожидайте звонка от нас.</div>-->
<div style="text-align: center; color: #EEE; font-size: 36px; font-weight: bold;">1250 руб.</div>
<div class="zakaz"><a href="#dialog-full" name="modal-full">Заказать!</a></div>

</div>
</div>


<div id="boxes-full">
	<div id="dialog-full" class="window-full">
	<div class="call-form-fon-full">
<div align="center" style="font-size: 20px; text-shadow: 0 1px #DDD;"><b>Заказать товар сейчас!</b></div><br />

<form class="cmxform" id="commentForm2" method="post" action="call.php" onsubmit="yaCounter28059519.reachGoal('Order'); return true;">
				<label>Ваше имя: </label>
				<input id="cename" name="name" class="winput" data-rule-required="true" data-msg-required="Пожалуйса введите Ваше имя" data-msg-name="Пожалуйса введите Ваше имя">
                <div class="call-name">Пример: Николай</div>
				
				<label>Ваш номер телефона: </label>
				<input id="cephone" name="phone" class="winput" data-rule-required="true" data-msg-required="Пожалуйса введите Ваш номер телефона" data-msg-phone="Пожалуйса введите Ваш номер телефона">
                <div class="call-phone">Пример: +7985671526</div>
				
				<label>Укажите удобный способ получения товара:*</label>
				<div class="get-full"><input type="radio" name="get" value="Самовывоз по Москве" />Самовывоз по Москве</div>
				<div class="get-full-opis">( * оплата наличными в пункте получения)</div>
				<div class="get-full"><input type="radio" name="get" value="Доставка по Москве курьером" />Доставка по Москве курьером</div>
				<div class="get-full-opis">( * оплата курьеру наличными после доставки)</div>
				<div class="get-full"><input type="radio" name="get" value="Доставка по России" />Доставка по России</div>
				<div class="get-full-opis">( * Оплата при получении товара на почте, о способах оплаты читайте в разделе "Оплата и доставка")</div>

				<input type="submit" class="send-zakaz" value="Заказать!" />

	</form>
</div>	<a href="#" class="close-full">Закрыть</a>
	</div>
	<div id="mask-full"></div>
</div>
<!-- Конец кода - Модальное окно с формой заказа товара -->
	
	
	
	<br />
	<div style="margin: -7px 0 0 200px; font-size: 25px; color: #00cc00; text-shadow: 1px 1px 2px black, 0 0 7px black; text-transform: uppercase;">До конца акции осталось:</div>
	<div style="margin: 0px 0 0 90px; background: #cfd1fa url(images/30.png); padding: 3px; border-radius: 15px;">
	<div id="countbox"></div>
	</div>
	</div>
</div>


<div class="slider">
	<ul>
		<li><img src="i/1.jpg" alt=""></li>
		<li><img src="i/2.jpg" alt=""></li>
		<li><img src="i/3.jpg" alt=""></li>

	</ul>
</div>


<div class="content1">

<div id="opisinie"></div>
<div class="title-razdel">
Описание МОНОПОДА модель z07-5
</div>
<div class="text">

<div align="center"><img src="/screen/z075.jpg" width="430px"></div>
<br />
<div style="padding-left: 15px; border-left: 3px solid #ff9900;">Итак, МОНОПОД модель z075 - гаджет для селфи. Благодаря ему у Вас будут крутые снимки.</div><br />

<div style="padding-left: 15px; border-left: 3px solid #ff9900;">Больше никого не надо просить. Снимайте сами и выкладывайте фото сразу, получайте сотни новых лайков!</div><br />

<div style="padding-left: 15px; border-left: 3px solid #ff9900;">Это аксессуар, который поднимет Ваше настроение.
</div><br />

<div style="padding-left: 15px; border-left: 3px solid #ff9900;">Только представьте, с каких восхитительных ракурсов Вы сможете снимать! Групповые снимки, селфи, концерты.</div><br />

<div style="padding-left: 15px; border-left: 3px solid #ff9900;">Теперь Вам не нужно будет мучиться в поисках места, откуда можно будет удобно сфотографироваться.</div><br />

<div style="padding-left: 15px; border-left: 3px solid #ff9900;">А еще монопод станет отличным подарком Вашему любимому человеку или близким. Подумайте, сколько отличных фотографий Вы сможете сделать вместе!</div>

</div>

<!-- Модальное окно с формой заказа товара -->
<a href="#dialog-full" name="modal-full"><div class="button-zakaz"></div></a>
<div id="boxes-full">
	<div id="dialog-full" class="window-full">
	<div class="call-form-fon-full">
<div align="center" style="font-size: 20px; text-shadow: 0 1px #DDD;"><b>Заказать товар сейчас!</b></div><br />

<form class="cmxform" id="commentForm2" method="post" action="call.php" onsubmit="yaCounter28059519.reachGoal('Order'); return true;">
				<label>Ваше имя: </label>
				<input id="cename" name="name" class="winput" data-rule-required="true" data-msg-required="Пожалуйса введите Ваше имя" data-msg-name="Пожалуйса введите Ваше имя">
                <div class="call-name">Пример: Николай</div>
				
				<label>Ваш номер телефона: </label>
				<input id="cephone" name="phone" class="winput" data-rule-required="true" data-msg-required="Пожалуйса введите Ваш номер телефона" data-msg-phone="Пожалуйса введите Ваш номер телефона">
                <div class="call-phone">Пример: +7985671526</div>
				
				<label>Укажите удобный способ получения товара:*</label>
				<div class="get-full"><input type="radio" name="get" value="Самовывоз по Москве" />Самовывоз по Москве</div>
				<div class="get-full-opis">( * оплата наличными в пункте получения)</div>
				<div class="get-full"><input type="radio" name="get" value="Доставка по Москве курьером" />Доставка по Москве курьером</div>
				<div class="get-full-opis">( * оплата курьеру наличными после доставки)</div>
				<div class="get-full"><input type="radio" name="get" value="Доставка по России" />Доставка по России</div>
				<div class="get-full-opis">( * Оплата при получении товара на почте, о способах оплаты читайте в разделе "Оплата и доставка")</div>

				<input type="submit" class="send-zakaz" value="Заказать!" />

	</form>
</div>	<a href="#" class="close-full">Закрыть</a>
	</div>
	<div id="mask-full"></div>
</div>
<!-- Конец кода - Модальное окно с формой заказа товара -->

<div id="ucl"></div>
<div class="title-razdel">
Подарок Чудо-Кнопка Ikey!
</div>
<div class="text">

<div align="center"><img src="/screen/ikey.jpg" width="430px"></div>
<br />
<div style="padding-left: 15px; border-left: 3px solid #ff9900;">iKey - 3,5-мм штекер для смартфонов/планшетов </div><br />

<div style="padding-left: 15px; border-left: 3px solid #ff9900;">Позволяет быстро перейти на нужное приложение: фото, видео, звонок, сайт Bконтакте и тд. То, что Вы чаще всего используете. </div><br />

<div style="padding-left: 15px; border-left: 3px solid #ff9900;">Достаточно сделать 1 нажатие, и переход будет осуществлен, звонок набран или запись видео включена.</div><br />
<div align="center"><img src="/screen/ikey2.jpg" width="430px"></div><br />

<div style="padding-left: 15px; border-left: 3px solid #ff9900;">Все просто! С помощью установленной в смартфон специальной программы программируете действие по количеству нажатий (например, вкл/выкл фонарик, ответ/сброс звонка, фотографирование без включения экрана, запись видео, запуск приложения и т.п.)</div><br />
<div align="center"><img src="/screen/ikey3.jpg" width="430px"></div><br />

<div style="padding-left: 15px; text-align: center;">Заполняйте форму заказа <br />
и получите многозадачную-кнопку IKey в подарок!
</div>
<!--<div align="center"><img src="/screen/Macro.jpg" width="430px"></div><br />-->

</div>

<!-- Модальное окно с формой заказа товара -->
<a href="#dialog-full" name="modal-full"><div class="button-zakaz"></div></a>
<div id="boxes-full">
	<div id="dialog-full" class="window-full">
	<div class="call-form-fon-full">
<div align="center" style="font-size: 20px; text-shadow: 0 1px #DDD;"><b>Заказать товар сейчас!</b></div><br />

<form class="cmxform" id="commentForm2" method="post" action="call.php" onsubmit="yaCounter28059519.reachGoal('Order'); return true;">
				<label>Ваше имя: </label>
				<input id="cename" name="name" class="winput" data-rule-required="true" data-msg-required="Пожалуйса введите Ваше имя" data-msg-name="Пожалуйса введите Ваше имя">
                <div class="call-name">Пример: Николай</div>
				
				<label>Ваш номер телефона: </label>
				<input id="cephone" name="phone" class="winput" data-rule-required="true" data-msg-required="Пожалуйса введите Ваш номер телефона" data-msg-phone="Пожалуйса введите Ваш номер телефона">
                <div class="call-phone">Пример: +7985671526</div>
				
				<label>Укажите удобный способ получения товара:*</label>
				<div class="get-full"><input type="radio" name="get" value="Самовывоз по Москве" />Самовывоз по Москве</div>
				<div class="get-full-opis">( * оплата наличными в пункте получения)</div>
				<div class="get-full"><input type="radio" name="get" value="Доставка по Москве курьером" />Доставка по Москве курьером</div>
				<div class="get-full-opis">( * оплата курьеру наличными после доставки)</div>
				<div class="get-full"><input type="radio" name="get" value="Доставка по России" />Доставка по России</div>
				<div class="get-full-opis">( * Оплата при получении товара на почте, о способах оплаты читайте в разделе "Оплата и доставка")</div>

				<input type="submit" class="send-zakaz" value="Заказать!" />

	</form>
</div>	<a href="#" class="close-full">Закрыть</a>
	</div>
	<div id="mask-full"></div>
</div>
<!-- Конец кода - Модальное окно с формой заказа товара -->


<div id="video"></div>
<div class="title-razdel">
Посмотреть, как работает Монопод z07-5<br />и его преимущества
</div>

<table border="0" width="100%" style="border-collapse: collapse">
	<tr>
		<td width="680" style="padding-left: 15px;">
			<iframe width="640" height="360" src="//www.youtube.com/embed/p9axMzSXjPE?feature=player_detailpage" frameborder="0" allowfullscreen></iframe>
		</td>
		<td>
			<div class="galka galka-text">Сделан из прочных материалов.</div>
			<div class="galka galka-text">Быстро собирается и разбирается.</div>
			<div class="galka galka-text">Встроенный Bluetooth.</div>
			<div class="galka galka-text">Прост в использовании.</div>
			<div class="galka galka-text">На выбор четыре цвета.</div>
			<div class="galka galka-text">Лёгкий и удобный.</div>
		</td>
	</tr>
</table>


<!-- Модальное окно с формой заказа товара -->
<br />
<a href="#dialog-full" name="modal-full"><div class="button-zakaz"></div></a>
<div id="boxes-full">
	<div id="dialog-full" class="window-full">
	<div class="call-form-fon-full">
<div align="center" style="font-size: 20px; text-shadow: 0 1px #DDD;"><b>Заказать товар сейчас!</b></div><br />

<form class="cmxform" id="commentForm2" method="post" action="call.php" onsubmit="yaCounter28059519.reachGoal('Order'); return true;">
				<label>Ваше имя: </label>
				<input id="cename" name="name" class="winput" data-rule-required="true" data-msg-required="Пожалуйса введите Ваше имя" data-msg-name="Пожалуйса введите Ваше имя">
                <div class="call-name">Пример: Николай</div>
				
				<label>Ваш номер телефона: </label>
				<input id="cephone" name="phone" class="winput" data-rule-required="true" data-msg-required="Пожалуйса введите Ваш номер телефона" data-msg-phone="Пожалуйса введите Ваш номер телефона">
                <div class="call-phone">Пример: +7985671526</div>
				
				<label>Укажите удобный способ получения товара:*</label>
				<div class="get-full"><input type="radio" name="get" value="Самовывоз по Москве" />Самовывоз по Москве</div>
				<div class="get-full-opis">( * оплата наличными в пункте получения)</div>
				<div class="get-full"><input type="radio" name="get" value="Доставка по Москве курьером" />Доставка по Москве курьером</div>
				<div class="get-full-opis">( * оплата курьеру наличными после доставки)</div>
				<div class="get-full"><input type="radio" name="get" value="Доставка по России" />Доставка по России</div>
				<div class="get-full-opis">( * Оплата при получении товара на почте, о способах оплаты читайте в разделе "Оплата и доставка")</div>

				<input type="submit" class="send-zakaz" value="Заказать!" />

	</form>
</div>	<a href="#" class="close-full">Закрыть</a>
	</div>
	<div id="mask-full"></div>
</div>
<!-- Конец кода - Модальное окно с формой заказа товара -->


<div id="otzivi"></div>
<div class="title-razdel">
Посмотрите, что говорят наши покупатели
</div>

<table border="0" width="100%" style="border-collapse: collapse">
	<tr>
		<td align="center" width="30%"><img src="/screen/01.jpg" height="100px"><br />Елена г. Люберцы</td>
		<td class="font">...Хочу выразить огромную благодарность!Осталась очень довольна моноподом!\Оперативность доставки и качество на 5+)оплатить можно при получении, что также очень удобно!Возьму с собой в отпуск,для качественных снимков!
</td>
	</tr>
	<tr>
		<td align="center" width="30%"><img src="/screen/02.jpg" height="100px"><br />Андрей г. Москва</td>
		<td class="font">Купил монопод здесь ! Без него теперь никуда !!! Супер вещь! А в Берлине все вообще были в шоке от нашей штуки))), там таких нет!</td>
	</tr>

</table>

<!-- Модальное окно с формой заказа товара -->
<br />
<a href="#dialog-full" name="modal-full"><div class="button-zakaz"></div></a>
<div id="boxes-full">
	<div id="dialog-full" class="window-full">
	<div class="call-form-fon-full">
<div align="center" style="font-size: 20px; text-shadow: 0 1px #DDD;"><b>Заказать товар сейчас!</b></div><br />

<form class="cmxform" id="commentForm2" method="post" action="call.php" onsubmit="yaCounter28059519.reachGoal('Order'); return true;">
				<label>Ваше имя: </label>
				<input id="cename" name="name" class="winput" data-rule-required="true" data-msg-required="Пожалуйса введите Ваше имя" data-msg-name="Пожалуйса введите Ваше имя">
                <div class="call-name">Пример: Николай</div>
				
				<label>Ваш номер телефона: </label>
				<input id="cephone" name="phone" class="winput" data-rule-required="true" data-msg-required="Пожалуйса введите Ваш номер телефона" data-msg-phone="Пожалуйса введите Ваш номер телефона">
                <div class="call-phone">Пример: +7985671526</div>
				
				<label>Укажите удобный способ получения товара:*</label>
				<div class="get-full"><input type="radio" name="get" value="Самовывоз по Москве" />Самовывоз по Москве</div>
				<div class="get-full-opis">( * оплата наличными в пункте получения)</div>
				<div class="get-full"><input type="radio" name="get" value="Доставка по Москве курьером" />Доставка по Москве курьером</div>
				<div class="get-full-opis">( * оплата курьеру наличными после доставки)</div>
				<div class="get-full"><input type="radio" name="get" value="Доставка по России" />Доставка по России</div>
				<div class="get-full-opis">( * Оплата при получении товара на почте, о способах оплаты читайте в разделе "Оплата и доставка")</div>

				<input type="submit" class="send-zakaz" value="Заказать!" />

	</form>
</div>	<a href="#" class="close-full">Закрыть</a>
	</div>
	<div id="mask-full"></div>
</div>
<!-- Конец кода - Модальное окно с формой заказа товара -->


<div id="charakteristiki"></div>
<div class="title-razdel">
Характеристики Монопода z07-5
</div>

<div style="position: absolute; margin-left: 640px; margin-top: 100px;"><img src="/screen/z075.png" width="430px"></div>

<div class="text">
<div style="padding-left: 15px; border-left: 3px solid #ff9900;"><font color="#cc6600">№1:</font> Забудьте про таймер. Просто нажмите кнопку на ручке в нужный для вас момент.</div><br />
<div style="padding-left: 15px; border-left: 3px solid #ff9900;"><font color="#cc6600">№2:</font> Прочный. Сделан из нержавеющей стали.</div><br />
<div style="padding-left: 15px; border-left: 3px solid #ff9900;"><font color="#cc6600">№3:</font> Работает как с iPhone, так и с Android-смартфонами.</div><br />
<div style="padding-left: 15px; border-left: 3px solid #ff9900;"><font color="#cc6600">№4:</font> Время работы 100 часов, а зарядки всего 1 час!</div><br />
<div style="padding-left: 15px; border-left: 3px solid #ff9900;"><font color="#cc6600">№5:</font> Выдвигается на метр в длину.</div><br />
<div style="padding-left: 15px; border-left: 3px solid #ff9900;"><font color="#cc6600">№6:</font> Встроенный Bluetooth.</div><br />
<div style="padding-left: 15px; border-left: 3px solid #ff9900;"><font color="#cc6600">№7:</font> Лёгкий. Вес всего 178 граммов!</div><br />
<div style="padding-left: 15px; border-left: 3px solid #ff9900;"><font color="#cc6600">№8:</font> Удобно держать в руке.</div><br />
<div style="padding-left: 15px; border-left: 3px solid #ff9900;"><font color="#cc6600">№9:</font> Стильный. 4 цвета. Выберите тот, что подходит именно вам.</div><br />
<div style="padding-left: 15px; border-left: 3px solid #ff9900;"><font color="#cc6600">№10:</font> Снимайте с любых ракурсов.</div><br />
<div style="padding-left: 15px; border-left: 3px solid #ff9900;"><font color="#cc6600">№11:</font> Можно снимать ещё и видео.</div><br />
<div style="padding-left: 15px; border-left: 3px solid #ff9900;"><font color="#cc6600">№12:</font> Подходит для любых телефонов, камер Go Pro и фотоаппаратов.</div><br />

С помощью нашего монопода можно делать не только селфи! Вы без проблем сделаете отличные снимки с любого концерта, шоу или с вечеринки! Снимайте в самых труднодоступных местах! Наш монопод идеально подходит для динамической съемки!<br /><br />

<div class="galka galka-text"><font color="#16558E">Поддерживаемые ОС:</font> Andriod => 3.0, iOS => 4.0</div>
<div class="galka galka-text"><font color="#16558E">Длина телескопического монопода:</font> 20-107см</div>
<div class="galka galka-text"><font color="#16558E">Основные материалы:</font> нержавеющая сталь</div>
<div class="galka galka-text"><font color="#16558E">Аккумулятор емкость:</font> 45mAh</div>
<div class="galka galka-text"><font color="#16558E">Зарядное напряжение:</font> 5V</div>
<div class="galka galka-text"><font color="#16558E">Время зарядки:</font> 1 час</div>
<div class="galka galka-text"><font color="#16558E">Время работы в режиме ожидания:</font> > 100 часов</div>

Хотите попробовать? Тогда покупайте монопод по специальной цене. Ваша экономия при покупке монопода <b>700 рублей</b>, окончательная цена составляет <b>1250 рублей</b>.<br /><br />
Данное предложение ограничено временем акции - поторопитесь купить монопод по выгодной цене.
</div>

<!-- Модальное окно с формой заказа товара -->
<br />
<a href="#dialog-full" name="modal-full"><div class="button-zakaz"></div></a>
<div id="boxes-full">
	<div id="dialog-full" class="window-full">
	<div class="call-form-fon-full">
<div align="center" style="font-size: 20px; text-shadow: 0 1px #DDD;"><b>Заказать товар сейчас!</b></div><br />

<form class="cmxform" id="commentForm2" method="post" action="call.php" onsubmit="yaCounter28059519.reachGoal('Order'); return true;">
				<label>Ваше имя: </label>
				<input id="cename" name="name" class="winput" data-rule-required="true" data-msg-required="Пожалуйса введите Ваше имя" data-msg-name="Пожалуйса введите Ваше имя">
                <div class="call-name">Пример: Николай</div>
				
				<label>Ваш номер телефона: </label>
				<input id="cephone" name="phone" class="winput" data-rule-required="true" data-msg-required="Пожалуйса введите Ваш номер телефона" data-msg-phone="Пожалуйса введите Ваш номер телефона">
                <div class="call-phone">Пример: +7985671526</div>
				
				<label>Укажите удобный способ получения товара:*</label>
				<div class="get-full"><input type="radio" name="get" value="Самовывоз по Москве" />Самовывоз по Москве</div>
				<div class="get-full-opis">( * оплата наличными в пункте получения)</div>
				<div class="get-full"><input type="radio" name="get" value="Доставка по Москве курьером" />Доставка по Москве курьером</div>
				<div class="get-full-opis">( * оплата курьеру наличными после доставки)</div>
				<div class="get-full"><input type="radio" name="get" value="Доставка по России" />Доставка по России</div>
				<div class="get-full-opis">( * Оплата при получении товара на почте, о способах оплаты читайте в разделе "Оплата и доставка")</div>

				<input type="submit" class="send-zakaz" value="Заказать!" />

	</form>
</div>	<a href="#" class="close-full">Закрыть</a>
	</div>
	<div id="mask-full"></div>
</div>
<!-- Конец кода - Модальное окно с формой заказа товара -->




<div id="work"></div>
<div class="title-razdel">
Как пользоваться Моноподом z07-5?
</div>

<div class="text">
<div style="padding-left: 15px; border-left: 3px solid #ff9900;">Конструкция МОНОПОДА представляет собой раздвижную штангу, на которую устанавливается крепление для телефона или камеры. Аксессуар достаточно легкий, его вес без камеры всего 140 г. Очень удобно, ведь рука не устанет держать штатив во время продолжительного селфи.</div>
</div>

<table border="0" width="100%" style="border-collapse: collapse">
	<tr>
		<td width="25%" align="center"><div align="center"><img src="/screen/work1.png"></div></td>
		<td width="25%" align="center"><div align="center"><img src="/screen/work2.png"></div></td>
		<td width="25%" align="center"><div align="center"><img src="/screen/work3.png"></div></td>
		<td width="25%" align="center"><div align="center"><img src="/screen/work4.png"></div></td>
	</tr>
	<tr>
		<td width="25%" class="text-table" valign="top">Монопод легко собирается и разбирается</td>
		<td width="25%" class="text-table" valign="top">Закрепляете свой смартфон на моноподе</td>
		<td width="25%" class="text-table" valign="top">Выдвигаете монопод на нужную длину, ищите подходящий ракурс</td>
		<td width="25%" class="text-table" valign="top">Нажимаете кнопку на ручке монопода - идеальный снимок ГОТОВ!</td>
	</tr>
</table>

<!-- Модальное окно с формой заказа товара -->
<br />
<a href="#dialog-full" name="modal-full"><div class="button-zakaz"></div></a>
<div id="boxes-full">
	<div id="dialog-full" class="window-full">
	<div class="call-form-fon-full">
<div align="center" style="font-size: 20px; text-shadow: 0 1px #DDD;"><b>Заказать товар сейчас!</b></div><br />

<form class="cmxform" id="commentForm2" method="post" action="call.php" onsubmit="yaCounter28059519.reachGoal('Order'); return true;">
				<label>Ваше имя: </label>
				<input id="cename" name="name" class="winput" data-rule-required="true" data-msg-required="Пожалуйса введите Ваше имя" data-msg-name="Пожалуйса введите Ваше имя">
                <div class="call-name">Пример: Николай</div>
				
				<label>Ваш номер телефона: </label>
				<input id="cephone" name="phone" class="winput" data-rule-required="true" data-msg-required="Пожалуйса введите Ваш номер телефона" data-msg-phone="Пожалуйса введите Ваш номер телефона">
                <div class="call-phone">Пример: +7985671526</div>
				
				<label>Укажите удобный способ получения товара:*</label>
				<div class="get-full"><input type="radio" name="get" value="Самовывоз по Москве" />Самовывоз по Москве</div>
				<div class="get-full-opis">( * оплата наличными в пункте получения)</div>
				<div class="get-full"><input type="radio" name="get" value="Доставка по Москве курьером" />Доставка по Москве курьером</div>
				<div class="get-full-opis">( * оплата курьеру наличными после доставки)</div>
				<div class="get-full"><input type="radio" name="get" value="Доставка по России" />Доставка по России</div>
				<div class="get-full-opis">( * Оплата при получении товара на почте, о способах оплаты читайте в разделе "Оплата и доставка")</div>

				<input type="submit" class="send-zakaz" value="Заказать!" />

	</form>
</div>	<a href="#" class="close-full">Закрыть</a>
	</div>
	<div id="mask-full"></div>
</div>
<!-- Конец кода - Модальное окно с формой заказа товара -->


<div id="vigodi"></div>
<div class="title-razdel">
Четыре причины почему Монопод z07-5<br />стоит покупать у нас!
</div>

<table border="0" width="97%" style="border-collapse: collapse; font-size: 20px;" align="center" bordercolorlight="#888888" bordercolordark="#888888">

	<tr>
		<td align="center" width="25%"><img src="/images/checked.png"></td>
		<td align="center" width="25%"><img src="/images/pay.png"></td>
		<td align="center" width="25%"><img src="/images/back_money.png"></td>
		<td align="center" width="25%"><img src="/images/phone.png"></td>
	</tr>
	<tr>
		<td align="center" width="25%">Только проверенный товар - перед отправкой проверяется нашими специалистами</td>
		<td align="center" width="25%">Любая форма оплаты - электронные деньги, оплата на расчетный счет, оплата курьеру при получении</td>
		<td align="center" width="25%">Гарантия возрата денег 100%: Если вам не подошел товар или в случае поломки!</td>
		<td align="center" width="25%">Моментально отвечаем на Ваш звонок или сами перезваниваем в течение 5 минут</td>
	</tr>
</table>

<br />
<div class="text">
<b>Кроме этого:</b><br />
<table width="100%">
	<tr>
		<td width="50%" valign="top"><font size="5">
1. Работаем напрямую с заводом изготовителем.<br />
2. Доставка по Москве день в день или на следующий.<br />
3. Быстрая доставка по всей России.<br />
4. Никакой предоплаты, оплата при получении.<br />
		</font></td>
		<td width="50%" valign="top"><font size="5">
5. Только проверенный товар.<br />
6. 100 % гарантия.<br />
7. Тест-драйв на месте доставки.<br />
8. Доставка и оплата по всей России.
		</font></td>
	</tr>
</table>
</div> 


<!-- Модальное окно с формой заказа товара -->
<br />
<a href="#dialog-full" name="modal-full"><div class="button-zakaz"></div></a>
<div id="boxes-full">
	<div id="dialog-full" class="window-full">
	<div class="call-form-fon-full">
<div align="center" style="font-size: 20px; text-shadow: 0 1px #DDD;"><b>Заказать товар сейчас!</b></div><br />

<form class="cmxform" id="commentForm2" method="post" action="call.php" onsubmit="yaCounter28059519.reachGoal('Order'); return true;">
				<label>Ваше имя: </label>
				<input id="cename" name="name" class="winput" data-rule-required="true" data-msg-required="Пожалуйса введите Ваше имя" data-msg-name="Пожалуйса введите Ваше имя">
                <div class="call-name">Пример: Николай</div>
				
				<label>Ваш номер телефона: </label>
				<input id="cephone" name="phone" class="winput" data-rule-required="true" data-msg-required="Пожалуйса введите Ваш номер телефона" data-msg-phone="Пожалуйса введите Ваш номер телефона">
                <div class="call-phone">Пример: +7985671526</div>
				
				<label>Укажите удобный способ получения товара:*</label>
				<div class="get-full"><input type="radio" name="get" value="Самовывоз по Москве" />Самовывоз по Москве</div>
				<div class="get-full-opis">( * оплата наличными в пункте получения)</div>
				<div class="get-full"><input type="radio" name="get" value="Доставка по Москве курьером" />Доставка по Москве курьером</div>
				<div class="get-full-opis">( * оплата курьеру наличными после доставки)</div>
				<div class="get-full"><input type="radio" name="get" value="Доставка по России" />Доставка по России</div>
				<div class="get-full-opis">( * Оплата при получении товара на почте, о способах оплаты читайте в разделе "Оплата и доставка")</div>

				<input type="submit" class="send-zakaz" value="Заказать!" />

	</form>
</div>	<a href="#" class="close-full">Закрыть</a>
	</div>
	<div id="mask-full"></div>
</div>
<!-- Конец кода - Модальное окно с формой заказа товара -->

<div id="buy"></div>
<div class="title-razdel">
Покупка и доставка
</div>

<div style="position: absolute; margin-left: 730px;;"><img src="/screen/dostavka.png"></div>

<div class="galka galka-text">1) Нажмите кнопку ЗАКАЗАТЬ или закажите ОБРАТНЫЙ ЗВОНОК.</div>
<div class="galka galka-text">2) Наш менеджер перезванивает Вам.</div>
<div class="galka galka-text">3) Мы согласуем время доставки</div>
<div class="galka galka-text">4) Наш курьер вовремя привезет заказ Вам.</div>
<div class="galka galka-text">5) Вы производите тест-драйв на месте доставки и получаете все документы.</div>
<div class="galka galka-text">6) Вам останется только радоваться замечательной покупке.</div>


<div class="text-center">
<b>Доставка и оплата по всей России<br /><br />
<img src="/screen/Russia.jpg" width="430px"><br /><br />
...............по Москве...............</b><br />
</div>
<div class="text">Доставка по Москве день в день или на следующий<br /> 
Стоимость обычной доставки по г. Москве в пределах МКАД: 250 рублей.<br /> 
<br />
Стоимость доставки за пределы МКАД: 250 руб + 20 рублей за 1 км<br />
(Стоимость согласовуется с диспетчером интернет-магазина)
</div>
 
<div class="text-center">
<b>....................Самовывоз....................</b><br />
</div>

<div class="text">
Самовывоз со склада в Москве: бесплатно.<br /> 
Адреса и время работы уточняйте у операторов.<br /><br />

<div class="text">
<div style="padding-left: 15px; border-left: 3px solid #ff9900;">Доставка по России производится почтой России 1 класса или транспортной компанией «Деловые линии».</div><br />
<div style="padding-left: 15px; border-left: 3px solid #ff9900;">Доставим быстро и в срок.</div><br />
<div style="padding-left: 15px; border-left: 3px solid #ff9900;">Милости просим в наш магазин.</div><br />
</div>
</div>


<div id="guarantee"></div>
<div class="title-razdel">
Гарантия
</div>

<div class="text-center">
<img src="/screen/guarantee.png" width="430px" />
<!--<p>Мы представляем настоящий и проверенный монопод, который покупаем напрямую у производителя.</p>Вы можете быть уверены в качестве его работы и сроке службы. 
Но если товар Вам все таки не понравился, мы гарантируем, что вернем Вам деньги без экспертизы и заявлений. Просто потому, что он Вам не понравился.--><p>В течении 14 дней Вы можете его вернуть, ничего не объясняя. Но есть одно условие - товар и упаковка должны быть не повреждены.</p>
Мы готовы ответить на любые ваши вопросы по телефону 8 499 346 69 35
</div>



<!-- Модальное окно с формой заказа товара -->
<br />
<a href="#dialog-full" name="modal-full"><div class="button-zakaz"></div></a>
<div id="boxes-full">
	<div id="dialog-full" class="window-full">
	<div class="call-form-fon-full">
<div align="center" style="font-size: 20px; text-shadow: 0 1px #DDD;"><b>Заказать товар сейчас!</b></div><br />

<form class="cmxform" id="commentForm2" method="post" action="call.php" onsubmit="yaCounter28059519.reachGoal('Order'); return true;">
				<label>Ваше имя: </label>
				<input id="cename" name="name" class="winput" data-rule-required="true" data-msg-required="Пожалуйса введите Ваше имя" data-msg-name="Пожалуйса введите Ваше имя">
                <div class="call-name">Пример: Николай</div>
				
				<label>Ваш номер телефона: </label>
				<input id="cephone" name="phone" class="winput" data-rule-required="true" data-msg-required="Пожалуйса введите Ваш номер телефона" data-msg-phone="Пожалуйса введите Ваш номер телефона">
                <div class="call-phone">Пример: +7985671526</div>
				
				<label>Укажите удобный способ получения товара:*</label>
				<div class="get-full"><input type="radio" name="get" value="Самовывоз по Москве" />Самовывоз по Москве</div>
				<div class="get-full-opis">( * оплата наличными в пункте получения)</div>
				<div class="get-full"><input type="radio" name="get" value="Доставка по Москве курьером" />Доставка по Москве курьером</div>
				<div class="get-full-opis">( * оплата курьеру наличными после доставки)</div>
				<div class="get-full"><input type="radio" name="get" value="Доставка по России" />Доставка по России</div>
				<div class="get-full-opis">( * Оплата при получении товара на почте, о способах оплаты читайте в разделе "Оплата и доставка")</div>

				<input type="submit" class="send-zakaz" value="Заказать!" />

	</form>
</div>	<a href="#" class="close-full">Закрыть</a>
	</div>
	<div id="mask-full"></div>
</div>
<!-- Конец кода - Модальное окно с формой заказа товара -->


<div class="text" style="background: #FFF; text-align: center; font-size: 30px; color: #cc0000;">
<b>И ВЫ ДО СИХ ПОР ДУМАЕТЕ?</b><br />
Акция скоро закончится, и Вы можете не успеть воспользоваться<br />нашим полезным и нужным бонусом.<br /><br />
Нажимайте кнопку <b>ЗАКАЗАТЬ СЕЙЧАС!</b><br />
и делайте неотразимые фото с помощью <span style="text-decoration: underline; color: #16558E"><b>Монопода z07-5 и Чудо-Кнопки IKey!</b></span>
</div>

<!-- Модальное окно с формой заказа товара -->
<br />
<a href="#dialog-full" name="modal-full"><div class="button-zakaz"></div></a>
<div id="boxes-full">
	<div id="dialog-full" class="window-full">
	<div class="call-form-fon-full">
<div align="center" style="font-size: 20px; text-shadow: 0 1px #DDD;"><b>Заказать товар сейчас!</b></div><br />

<form class="cmxform" id="commentForm2" method="post" action="call.php" onsubmit="yaCounter28059519.reachGoal('Order'); return true;">
				<label>Ваше имя: </label>
				<input id="cename" name="name" class="winput" data-rule-required="true" data-msg-required="Пожалуйса введите Ваше имя" data-msg-name="Пожалуйса введите Ваше имя">
                <div class="call-name">Пример: Николай</div>
				
				<label>Ваш номер телефона: </label>
				<input id="cephone" name="phone" class="winput" data-rule-required="true" data-msg-required="Пожалуйса введите Ваш номер телефона" data-msg-phone="Пожалуйса введите Ваш номер телефона">
                <div class="call-phone">Пример: +7985671526</div>
				
				<label>Укажите удобный способ получения товара:*</label>
				<div class="get-full"><input type="radio" name="get" value="Самовывоз по Москве" />Самовывоз по Москве</div>
				<div class="get-full-opis">( * оплата наличными в пункте получения)</div>
				<div class="get-full"><input type="radio" name="get" value="Доставка по Москве курьером" />Доставка по Москве курьером</div>
				<div class="get-full-opis">( * оплата курьеру наличными после доставки)</div>
				<div class="get-full"><input type="radio" name="get" value="Доставка по России" />Доставка по России</div>
				<div class="get-full-opis">( * Оплата при получении товара на почте, о способах оплаты читайте в разделе "Оплата и доставка")</div>

				<input type="submit" class="send-zakaz" value="Заказать!" />

	</form>
</div>	<a href="#" class="close-full">Закрыть</a>
	</div>
	<div id="mask-full"></div>
</div>
<!-- Конец кода - Модальное окно с формой заказа товара -->

</div>
<div class="footer">
<div class="text" style="width: 50%; float: left;"><font color="#cc6600"><b>КОНТАКТЫ:</b></font><br />E-mail: sales@new-monopod.ru<br />Телефон: 8 (499) 346-69-35</div>
<div style="float: right; width: 50%; margin-top: -100px; padding-right: 20px;">Copyright &copy;2015  Монопод  c  Bluetooth <br /> #ЛюблюСелфи <p>


</div>
</div>

<!-- Начало скрипта скрола через меню -->
<script type="text/javascript"> 
 $('#scrollPanel a').click(function(){ 
 var str=$(this).attr('href'); 
 $.scrollTo(str,500); 
 return false; 
 }); 
 </script>
<!-- Конец скрипта скрола через меню -->

<div class="share">

<script type="text/javascript">(function() {
  if (window.pluso)if (typeof window.pluso.start == "function") return;
  if (window.ifpluso==undefined) { window.ifpluso = 1;
    var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
    s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
    s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
    var h=d[g]('body')[0];
    h.appendChild(s);
  }})();</script>
<div class="pluso" data-background="transparent" data-options="big,round,line,vertical,counter,theme=03" data-services="vkontakte,odnoklassniki,facebook,twitter,google,moimir,email,print"></div>

</div>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter28059519 = new Ya.Metrika({id:28059519,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/28059519" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<link rel="stylesheet" href="//cdn.callbackhunter.com/widget/tracker.css"> <script type="text/javascript" src="//cdn.callbackhunter.com/widget/tracker.js" charset="UTF-8"></script>
<script type="text/javascript">var hunter_code="8c0049dc10de31debbee5cd2a4b80a9f";</script>
</body>
</html>